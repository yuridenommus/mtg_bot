{
  description = "A bot for searching MtG cards";

  inputs = {
    cargo2nix.url = "github:cargo2nix/cargo2nix/release-0.11.0";
    flake-utils.follows = "cargo2nix/flake-utils";
    nixpkgs.follows = "cargo2nix/nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils, cargo2nix, ... }:
  flake-utils.lib.eachDefaultSystem (system:
  let
    cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
    toolchainToml = builtins.fromTOML (builtins.readFile ./rust-toolchain.toml);
    name = cargoToml.package.name;
    binaryNames = builtins.catAttrs "name" (cargoToml.bin);
    pkgs = import nixpkgs {
      inherit system;
      overlays = [
        (final: prev: let
          pkg = final.rustPkgs.workspace."${name}" {};
        in {
          "${name}_docker" = final.dockerTools.buildImage {
            name = "registry.gitlab.com/yuridenommus/mtg_bot";
            tag = "latest";
            contents = [
              final."${name}"
              final.diesel-cli
              final.curl
              final.jq
              final.bash
              final.findutils
            ];
          };
          finalApps = final.lib.genAttrs binaryNames (n: {
            type = "app";
            program = "${pkgs."${n}"}/bin/${n}";
          });
          "${name}" = pkg.bin;
          diesel = (builtins.elemAt
          (builtins.attrValues final.rustPkgs."registry+https\://github.com/rust-lang/crates.io-index".diesel)
          0) {};
          "${name}-deps" = final.stdenv.mkDerivation {
            name = "${name}-deps";
            version = "0.1";
            installPhase = ''
              mkdir -p $out
              touch $out/${name}-deps
            '';
            src = ./Cargo.nix;
            unpackPhase = "true";
            propagatedBuildInputs = pkg.buildInputs
                                    ++ pkg.propagatedBuildInputs
                                    ++ pkg.nativeBuildInputs
                                    ++ pkg.propagatedNativeBuildInputs
                                    ++ [
                                      final.diesel-cli
                                    ];
          };
        })
        (final: prev: {
          diesel-cli = (builtins.elemAt
            (builtins.attrValues final.rustPkgs."registry+https\://github.com/rust-lang/crates.io-index".diesel_cli)
            0) {};
          postgresql = prev.postgresql.override {
            enableSystemd = false;
          };
        })
        (cargo2nix.overlays.default)
        (final: prev: {
          rustPkgs = final.rustBuilder.makePackageSet {
            rustVersion = toolchainToml.toolchain.channel;
            rustProfile = toolchainToml.toolchain.profile;
            extraRustComponents = toolchainToml.toolchain.components;
            packageFun = import ./Cargo.nix;
          };
        })
      ];
    };
  in
  {
    inherit pkgs; # for debugging
    packages = {
      "${name}" = pkgs."${name}";
      "${name}-deps" = pkgs."${name}-deps";
      default = pkgs."${name}";
      inherit (pkgs) diesel diesel-cli;
    } // (if pkgs.stdenv.isLinux then {
      "${name}_docker" = pkgs."${name}_docker";
    } else {});

    devShell = pkgs.rustPkgs.workspaceShell {
      buildInputs = with pkgs; [
        nixpkgs-fmt
        rust-analyzer
        diesel-cli
        postgresql
      ];
      shellHook = ''
        if [ ! -d ./postgres ]; then
        initdb -D ./postgres --auth-local=trust --no-locale --encoding=UTF8
        fi
        if [ ! -f ./postgres/postmaster.pid ]; then
        pg_ctl -D ./postgres start
        fi
        createdb mtg_bot || echo "Database exists"
        export DATABASE_URL=postgres://$USER@localhost/mtg_bot
      '';
    };
    apps = pkgs.finalApps;
  });
}
