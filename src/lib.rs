#![recursion_limit = "256"]
#[macro_use]
extern crate diesel;

use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use r2d2::Error;

pub mod models;
#[rustfmt::skip]
pub mod schema;

pub type ConnectionPool = Pool<ConnectionManager<PgConnection>>;

pub fn connection_pool(db_url: &str) -> Result<ConnectionPool, Error> {
    let manager = ConnectionManager::new(db_url);
    Pool::new(manager)
}

#[cfg(test)]
pub mod test_utils {
    use std::{env, sync::Once};

    use diesel::{Connection, PgConnection};

    static INIT: Once = Once::new();

    pub fn init_test_db() -> PgConnection {
        let mut db = None;
        INIT.call_once(|| {
            let db_url = env::var("DATABASE_URL").expect("DATABASE_URL should be set");
            db = Some(PgConnection::establish(&db_url).unwrap());
        });
        db.unwrap()
    }
}
