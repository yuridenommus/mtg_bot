use crate::schema::{card_faces, cards, previews};
use crate::ConnectionPool;

use std::error::Error;

use chrono::NaiveDate;
use diesel::query_builder::{Query, QueryFragment, QueryId};
use diesel::{sql_types, RunQueryDsl};
use diesel::{PgConnection, Queryable};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::ops::Deref;
use tokio_diesel::{AsyncError, AsyncRunQueryDsl};
use uuid::Uuid;

#[derive(Queryable, Associations, Debug)]
#[table_name = "card_faces"]
#[belongs_to(Card)]
pub struct CardFace {
    pub id: i32,
    pub artist: String,
    pub cmc: Option<f32>,
    pub color_indicator: Vec<String>,
    pub colors: Vec<String>,
    pub flavor_text: String,
    pub illustration_id: Option<String>,
    pub image_uris: Value,
    pub layout: Option<String>,
    pub mana_cost: String,
    pub name: String,
    pub object: String,
    pub oracle_id: Option<String>,
    pub oracle_text: String,
    pub power: Option<String>,
    pub printed_name: String,
    pub printed_text: String,
    pub printed_type_line: String,
    pub toughness: Option<String>,
    pub type_line: String,
    pub watermark: String,
    pub card_id: Uuid,
}

#[derive(Queryable, Identifiable)]
#[table_name = "cards"]
pub struct Card {
    pub id: Uuid,
    pub arena_id: Option<i32>,
    pub lang: String,
    pub mtgo_id: Option<i32>,
    pub mtgo_foil_id: Option<i32>,
    pub multiverse_ids: Vec<i32>,
    pub tcgplayer_id: Option<i32>,
    pub tcgplayer_etched_id: Option<i32>,
    pub cardmarket_id: Option<i32>,
    pub object: String,
    pub oracle_id: Option<Uuid>,
    pub prints_search_uri: String,
    pub rulings_uri: String,
    pub scryfall_uri: String,
    pub uri: String,
    pub cmc: Option<f32>,
    pub color_identity: Vec<String>,
    pub color_indicator: Vec<String>,
    pub colors: Vec<String>,
    pub edhrec_rank: Option<i32>,
    pub hand_modifier: Option<String>,
    pub keywords: Vec<String>,
    pub layout: String,
    pub legalities: Option<Value>,
    pub life_modifier: Option<String>,
    pub loyalty: Option<String>,
    pub mana_cost: Option<String>,
    pub name: String,
    pub oracle_text: String,
    pub oversized: bool,
    pub power: Option<String>,
    pub produced_mana: Vec<String>,
    pub reserved: bool,
    pub toughness: Option<String>,
    pub type_line: String,
    pub artist: String,
    pub booster: bool,
    pub border_color: String,
    pub card_back_id: Option<Uuid>,
    pub collector_number: String,
    pub content_warning: bool,
    pub digital: bool,
    pub finishes: Vec<String>,
    pub flavor_name: String,
    pub flavor_text: String,
    pub frame_effects: Vec<String>,
    pub frame: String,
    pub full_art: bool,
    pub games: Vec<String>,
    pub highres_image: bool,
    pub illustration_id: Option<Uuid>,
    pub image_status: String,
    pub image_uris: Value,
    pub prices: Value,
    pub printed_name: String,
    pub printed_text: String,
    pub printed_type_line: String,
    pub promo: bool,
    pub promo_types: Vec<String>,
    pub purchase_uris: Value,
    pub rarity: String,
    pub released_at: Option<NaiveDate>,
    pub reprint: bool,
    pub scryfall_set_uri: String,
    pub set_name: String,
    pub set_search_uri: String,
    pub set_type: String,
    pub set: String,
    pub set_id: Option<Uuid>,
    pub story_spotlight: bool,
    pub textless: bool,
    pub variation: bool,
    pub variation_of: Option<String>,
    pub security_stamp: Option<String>,
    pub watermark: String,
}

impl Card {
    fn by_name_query(
        text: &str,
        offset: i64,
        page_size: i64,
    ) -> impl Query<
        SqlType = (
            sql_types::Uuid,
            sql_types::Jsonb,
            sql_types::Text,
            sql_types::Text,
            sql_types::Text,
        ),
    > + AsyncRunQueryDsl<PgConnection, ConnectionPool>
           + QueryFragment<diesel::pg::Pg>
           + QueryId
           + RunQueryDsl<PgConnection> {
        use crate::schema::cards::dsl::*;
        use diesel::prelude::*;
        cards
            .filter(
                name.ilike(format!("%{}%", text))
                    .and(lang.eq("en"))
                    .and(image_status.ne("missing")),
            )
            .order_by(id)
            .limit(page_size)
            .offset(offset)
            .select((id, image_uris, name, oracle_text, scryfall_uri))
    }

    pub async fn by_name_async(
        db: &ConnectionPool,
        text: &str,
        offset: i64,
        page_size: i64,
    ) -> Result<Vec<QueryCard>, AsyncError> {
        let card_list = Self::by_name_query(text, offset, page_size)
            .load_async(db)
            .await?;
        Ok(card_list)
    }

    pub fn by_name(
        db: &PgConnection,
        text: &str,
        offset: i64,
        page_size: i64,
    ) -> Result<Vec<QueryCard>, Box<dyn Error>> {
        let card_list = Self::by_name_query(text, offset, page_size).load(db)?;
        Ok(card_list)
    }
}

pub type QueryCard = (Uuid, Value, String, String, String);

#[derive(Queryable, Associations, Debug)]
#[belongs_to(Card)]
#[table_name = "previews"]
pub struct Preview {
    pub previewed_at: NaiveDate,
    pub source_uri: String,
    pub source: String,
    pub card_id: Uuid,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JsonCardFace {
    #[serde(default)]
    pub artist: String,
    pub cmc: Option<f32>,
    #[serde(default)]
    pub color_indicator: Vec<String>,
    #[serde(default)]
    pub colors: Vec<String>,
    #[serde(default)]
    pub flavor_text: String,
    pub illustration_id: Option<Uuid>,
    #[serde(default)]
    pub image_uris: Value,
    #[serde(default)]
    pub layout: Option<String>,
    pub mana_cost: String,
    pub name: String,
    pub object: String,
    pub oracle_id: Option<Uuid>,
    #[serde(default)]
    pub oracle_text: String,
    pub power: Option<String>,
    #[serde(default)]
    pub printed_name: String,
    #[serde(default)]
    pub printed_text: String,
    #[serde(default)]
    pub printed_type_line: String,
    pub toughness: Option<String>,
    #[serde(default)]
    pub type_line: String,
    #[serde(default)]
    pub watermark: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JsonCard {
    pub id: Uuid,
    pub arena_id: Option<i32>,
    pub lang: String,
    pub mtgo_id: Option<i32>,
    pub mtgo_foil_id: Option<i32>,
    pub multiverse_ids: Vec<i32>,
    pub tcgplayer_id: Option<i32>,
    pub tcgplayer_etched_id: Option<i32>,
    pub cardmarket_id: Option<i32>,
    pub object: String,
    pub oracle_id: Option<Uuid>,
    pub prints_search_uri: String,
    pub rulings_uri: String,
    pub scryfall_uri: String,
    pub uri: String,
    pub cmc: Option<f32>,
    pub color_identity: Vec<String>,
    #[serde(default)]
    pub color_indicator: Vec<String>,
    #[serde(default)]
    pub colors: Vec<String>,
    pub edhrec_rank: Option<i32>,
    pub hand_modifier: Option<String>,
    pub keywords: Vec<String>,
    pub layout: String,
    pub legalities: Option<Value>,
    pub life_modifier: Option<String>,
    pub loyalty: Option<String>,
    pub mana_cost: Option<String>,
    pub name: String,
    #[serde(default)]
    pub oracle_text: String,
    pub oversized: bool,
    pub power: Option<String>,
    #[serde(default)]
    pub produced_mana: Vec<String>,
    pub reserved: bool,
    pub toughness: Option<String>,
    #[serde(default)]
    pub type_line: String,
    #[serde(default)]
    pub artist: String,
    pub booster: bool,
    pub border_color: String,
    pub card_back_id: Option<Uuid>,
    pub collector_number: String,
    #[serde(default)]
    pub content_warning: bool,
    pub digital: bool,
    pub finishes: Vec<String>,
    #[serde(default)]
    pub flavor_name: String,
    #[serde(default)]
    pub flavor_text: String,
    #[serde(default)]
    pub frame_effects: Vec<String>,
    pub frame: String,
    pub full_art: bool,
    pub games: Vec<String>,
    pub highres_image: bool,
    pub illustration_id: Option<Uuid>,
    pub image_status: String,
    #[serde(default)]
    pub image_uris: Value,
    pub prices: Value,
    #[serde(default)]
    pub printed_name: String,
    #[serde(default)]
    pub printed_text: String,
    #[serde(default)]
    pub printed_type_line: String,
    pub promo: bool,
    #[serde(default)]
    pub promo_types: Vec<String>,
    #[serde(default)]
    pub purchase_uris: Value,
    pub rarity: String,
    pub released_at: Option<NaiveDate>,
    pub reprint: bool,
    pub scryfall_set_uri: String,
    pub set_name: String,
    pub set_search_uri: String,
    pub set_type: String,
    pub set: String,
    pub set_id: Option<Uuid>,
    pub story_spotlight: bool,
    pub textless: bool,
    pub variation: bool,
    pub variation_of: Option<String>,
    pub security_stamp: Option<String>,
    #[serde(default)]
    pub watermark: String,
    #[serde(default)]
    pub card_faces: Vec<JsonCardFace>,
    pub preview: Option<JsonPreview>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JsonPreview {
    pub previewed_at: NaiveDate,
    pub source_uri: String,
    pub source: String,
}

#[derive(Insertable, AsChangeset)]
#[table_name = "card_faces"]
pub struct InsertCardFace<'a> {
    pub id: i32,
    pub artist: &'a str,
    pub cmc: Option<f32>,
    pub color_indicator: &'a [String],
    pub colors: &'a [String],
    pub flavor_text: &'a str,
    pub illustration_id: Option<&'a Uuid>,
    pub image_uris: &'a Value,
    pub layout: Option<&'a str>,
    pub mana_cost: &'a str,
    pub name: &'a str,
    pub object: &'a str,
    pub oracle_id: Option<&'a Uuid>,
    pub oracle_text: &'a str,
    pub power: Option<&'a str>,
    pub printed_name: &'a str,
    pub printed_text: &'a str,
    pub printed_type_line: &'a str,
    pub toughness: Option<&'a str>,
    pub type_line: &'a str,
    pub watermark: &'a str,
    pub card_id: &'a Uuid,
}

impl<'a> InsertCardFace<'a> {
    pub fn new(card_id: &'a Uuid, id: i32, vm: &'a JsonCardFace) -> Self {
        let JsonCardFace {
            artist,
            cmc,
            color_indicator,
            colors,
            flavor_text,
            illustration_id,
            image_uris,
            layout,
            mana_cost,
            name,
            object,
            oracle_id,
            oracle_text,
            power,
            printed_name,
            printed_text,
            printed_type_line,
            toughness,
            type_line,
            watermark,
        } = vm;
        InsertCardFace {
            id,
            artist,
            cmc: *cmc,
            color_indicator: color_indicator.deref(),
            colors,
            flavor_text,
            illustration_id: illustration_id.as_ref(),
            image_uris,
            layout: layout.as_deref(),
            mana_cost,
            name,
            object,
            oracle_id: oracle_id.as_ref(),
            oracle_text,
            power: power.as_deref(),
            printed_name,
            printed_text,
            printed_type_line,
            toughness: toughness.as_deref(),
            type_line,
            watermark,
            card_id,
        }
    }
}

#[derive(Identifiable, Insertable, AsChangeset)]
#[table_name = "cards"]
pub struct InsertCard<'a> {
    pub id: &'a Uuid,
    pub arena_id: Option<i32>,
    pub lang: &'a str,
    pub mtgo_id: Option<i32>,
    pub mtgo_foil_id: Option<i32>,
    pub multiverse_ids: &'a [i32],
    pub tcgplayer_id: Option<i32>,
    pub tcgplayer_etched_id: Option<i32>,
    pub cardmarket_id: Option<i32>,
    pub object: &'a str,
    pub oracle_id: Option<&'a Uuid>,
    pub prints_search_uri: &'a str,
    pub rulings_uri: &'a str,
    pub scryfall_uri: &'a str,
    pub uri: &'a str,
    pub cmc: Option<f32>,
    pub color_identity: &'a [String],
    pub color_indicator: &'a [String],
    pub colors: &'a [String],
    pub edhrec_rank: Option<i32>,
    pub hand_modifier: Option<&'a str>,
    pub keywords: &'a [String],
    pub layout: &'a str,
    pub legalities: Option<&'a Value>,
    pub life_modifier: Option<&'a str>,
    pub loyalty: Option<&'a str>,
    pub mana_cost: Option<&'a str>,
    pub name: &'a str,
    pub oracle_text: &'a str,
    pub oversized: bool,
    pub power: Option<&'a str>,
    pub produced_mana: &'a [String],
    pub reserved: bool,
    pub toughness: Option<&'a str>,
    pub type_line: &'a str,
    pub artist: &'a str,
    pub booster: bool,
    pub border_color: &'a str,
    pub card_back_id: Option<&'a Uuid>,
    pub collector_number: &'a str,
    pub content_warning: bool,
    pub digital: bool,
    pub finishes: &'a [String],
    pub flavor_name: &'a str,
    pub flavor_text: &'a str,
    pub frame_effects: &'a [String],
    pub frame: &'a str,
    pub full_art: bool,
    pub games: &'a [String],
    pub highres_image: bool,
    pub illustration_id: Option<&'a Uuid>,
    pub image_status: &'a str,
    pub image_uris: &'a Value,
    pub prices: &'a Value,
    pub printed_name: &'a str,
    pub printed_text: &'a str,
    pub printed_type_line: &'a str,
    pub promo: bool,
    pub promo_types: &'a [String],
    pub purchase_uris: &'a Value,
    pub rarity: &'a str,
    pub released_at: Option<NaiveDate>,
    pub reprint: bool,
    pub scryfall_set_uri: &'a str,
    pub set_name: &'a str,
    pub set_search_uri: &'a str,
    pub set_type: &'a str,
    pub set: &'a str,
    pub set_id: Option<&'a Uuid>,
    pub story_spotlight: bool,
    pub textless: bool,
    pub variation: bool,
    pub variation_of: Option<&'a str>,
    pub security_stamp: Option<&'a str>,
    pub watermark: &'a str,
}

impl<'a> InsertCard<'a> {
    pub fn new(c: &'a JsonCard) -> Self {
        let JsonCard {
            id,
            arena_id,
            lang,
            mtgo_id,
            mtgo_foil_id,
            multiverse_ids,
            tcgplayer_id,
            tcgplayer_etched_id,
            cardmarket_id,
            object,
            oracle_id,
            prints_search_uri,
            rulings_uri,
            scryfall_uri,
            uri,
            cmc,
            color_identity,
            color_indicator,
            colors,
            edhrec_rank,
            hand_modifier,
            keywords,
            layout,
            legalities,
            life_modifier,
            loyalty,
            mana_cost,
            name,
            oracle_text,
            oversized,
            power,
            produced_mana,
            reserved,
            toughness,
            type_line,
            artist,
            booster,
            border_color,
            card_back_id,
            collector_number,
            content_warning,
            digital,
            finishes,
            flavor_name,
            flavor_text,
            frame_effects,
            frame,
            full_art,
            games,
            highres_image,
            illustration_id,
            image_status,
            image_uris,
            prices,
            printed_name,
            printed_text,
            printed_type_line,
            promo,
            promo_types,
            purchase_uris,
            rarity,
            released_at,
            reprint,
            scryfall_set_uri,
            set_name,
            set_search_uri,
            set_type,
            set,
            set_id,
            story_spotlight,
            textless,
            variation,
            variation_of,
            security_stamp,
            watermark,
            ..
        } = c;
        InsertCard {
            id,
            arena_id: *arena_id,
            lang,
            mtgo_id: *mtgo_id,
            mtgo_foil_id: *mtgo_foil_id,
            multiverse_ids,
            tcgplayer_id: *tcgplayer_id,
            tcgplayer_etched_id: *tcgplayer_etched_id,
            cardmarket_id: *cardmarket_id,
            object,
            oracle_id: oracle_id.as_ref(),
            prints_search_uri,
            rulings_uri,
            scryfall_uri,
            uri,
            cmc: *cmc,
            color_identity: color_identity.deref(),
            color_indicator,
            colors,
            edhrec_rank: *edhrec_rank,
            hand_modifier: hand_modifier.as_deref(),
            keywords,
            layout,
            legalities: legalities.as_ref(),
            life_modifier: life_modifier.as_deref(),
            loyalty: loyalty.as_deref(),
            mana_cost: mana_cost.as_deref(),
            name,
            oracle_text,
            oversized: *oversized,
            power: power.as_deref(),
            produced_mana,
            reserved: *reserved,
            toughness: toughness.as_deref(),
            type_line,
            artist,
            booster: *booster,
            border_color,
            card_back_id: card_back_id.as_ref(),
            collector_number,
            content_warning: *content_warning,
            digital: *digital,
            finishes,
            flavor_name,
            flavor_text,
            frame_effects,
            frame,
            full_art: *full_art,
            games,
            highres_image: *highres_image,
            illustration_id: illustration_id.as_ref(),
            image_status,
            image_uris,
            prices,
            printed_name,
            printed_text,
            printed_type_line,
            promo: *promo,
            promo_types: promo_types.deref(),
            purchase_uris,
            rarity,
            released_at: *released_at,
            reprint: *reprint,
            scryfall_set_uri,
            set_name,
            set_search_uri,
            set_type,
            set,
            set_id: set_id.as_ref(),
            story_spotlight: *story_spotlight,
            textless: *textless,
            variation: *variation,
            variation_of: variation_of.as_deref(),
            security_stamp: security_stamp.as_deref(),
            watermark,
        }
    }
}

#[derive(Insertable, AsChangeset)]
#[table_name = "previews"]
pub struct InsertPreview<'a> {
    pub previewed_at: &'a NaiveDate,
    pub source_uri: &'a str,
    pub source: &'a str,
    pub card_id: &'a Uuid,
}

impl<'a> InsertPreview<'a> {
    pub fn new(card_id: &'a Uuid, vm: &'a JsonPreview) -> Self {
        let JsonPreview {
            previewed_at,
            source_uri,
            source,
        } = vm;
        InsertPreview {
            previewed_at,
            source_uri,
            source,
            card_id,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::test_utils::init_test_db;

    use super::{Card, InsertCard, JsonCard};

    use diesel::connection::Connection;

    #[test]
    fn search_sefris() {
        let db = init_test_db();
        let db_ref = &db;

        let sefris_json: JsonCard =
            reqwest::blocking::get("https://api.scryfall.com/cards/multiverse/531832")
                .unwrap()
                .json()
                .unwrap();
        let sefris_record = InsertCard::new(&sefris_json);

        db.test_transaction::<_, (), _>(move || {
            use crate::schema::cards::dsl::*;
            use diesel::prelude::*;

            diesel::insert_into(cards)
                .values(&sefris_record)
                .execute(db_ref)
                .unwrap();

            let sefris = Card::by_name(db_ref, "Sefris", 0, 30).expect("Could not find");
            assert_eq!(sefris.len(), 1);

            Ok(())
        });
    }
}
