#[macro_use]
extern crate rocket;

use std::error::Error;
use std::io::Cursor;
use std::{collections::HashMap, env};

use mtg_bot::models::QueryCard;
use mtg_bot::{connection_pool, models::Card, ConnectionPool};

use futures::stream::{self, StreamExt};
use image::io::Reader as ImageReader;
use rocket::{serde::json::Json, State};
use telegram_bot::{
    AnswerInlineQuery, Api, InlineQueryResult, InlineQueryResultPhoto, Update, UpdateKind,
};

const PAGE_SIZE: i64 = 30;

async fn diesel_to_telegram(card_list: Vec<QueryCard>) -> Vec<InlineQueryResult> {
    let client = reqwest::Client::new();
    let client_ref = &client;
    stream::iter(card_list.into_iter())
        .filter_map(
            |(id, image_uris, name, oracle_text, scryfall_uri)| async move {
                let large = image_uris["large"]
                    .as_str()
                    .or_else(|| image_uris["normal"].as_str())
                    .or_else(|| image_uris["small"].as_str())?
                    .to_string();
                let small = image_uris["small"].as_str()?.to_string();
                println!("Getting information from {}", large);
                let image_bytes = client_ref
                    .get(&large)
                    .send()
                    .await
                    .ok()?
                    .bytes()
                    .await
                    .ok()?;
                println!("Image bytes fetched");
                let (width, height) = tokio::task::spawn_blocking(move || {
                    ImageReader::new(Cursor::new(image_bytes))
                        .with_guessed_format()
                        .ok()?
                        .into_dimensions()
                        .ok()
                })
                .await
                .ok()
                .flatten()?;
                println!("Width and Height {}, {}", width, height);
                Some(InlineQueryResult::InlineQueryResultPhoto(
                    InlineQueryResultPhoto {
                        id: id.to_string(),
                        photo_url: large,
                        thumb_url: small,
                        photo_width: Some(width as i64),
                        photo_height: Some(height as i64),
                        title: Some(name),
                        description: Some(oracle_text),
                        caption: Some(scryfall_uri),
                        parse_mode: None,
                        reply_markup: None,
                        input_message_content: None,
                    },
                ))
            },
        )
        .collect()
        .await
}

#[post("/", data = "<update>")]
async fn index(db: &State<ConnectionPool>, api: &State<Api>, update: Json<Update>) -> Option<()> {
    // If the received update contains a new message...
    println!("Retrieving update");
    let update = update.into_inner();
    if let UpdateKind::InlineQuery(query) = update.kind {
        let offset: i64 = query.offset.parse().unwrap_or_default();
        println!(
            "Answering to user {:?} ({}, {}) with query {} and offset {}",
            query.from.username, query.from.first_name, query.from.id, query.query, offset
        );
        let cards = diesel_to_telegram(
            Card::by_name_async(db, &query.query, offset, PAGE_SIZE)
                .await
                .ok()?,
        )
        .await;
        println!("Obtained {} cards", cards.len());
        println!("Sending cards to user");
        match api
            .send(
                AnswerInlineQuery::new(query.id, cards)
                    .next_offset((offset + PAGE_SIZE + 1).to_string()),
            )
            .await
        {
            Ok(_) => println!("Success!"),
            Err(x) => println!("Error {}", x),
        }
        println!("End of query {}", query.query);
    }
    Some(())
}

#[rocket::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv::dotenv()?;
    println!("Checking telegram token");
    let token = env::var("TELEGRAM_BOT_TOKEN").or_else(|_| dotenv::var("TELEGRAM_BOT_TOKEN"))?;
    println!("Checking virtual host");
    let virtual_host = env::var("VIRTUAL_HOST").or_else(|_| dotenv::var("VIRTUAL_HOST"))?;
    println!("Checking database URL");
    let database_url = env::var("DATABASE_URL").or_else(|_| dotenv::var("DATABASE_URL"))?;
    println!("database url is {}", database_url);
    let db = connection_pool(&database_url)?;
    println!("Opened connection with database");
    let api = Api::new(token.as_str());

    // Fetch new updates via webhook
    let route = format!("/{}", token.as_str());
    let telegram_url = format!("https://api.telegram.org/bot{}/setWebhook", token);
    let url = format!("https://{}{}", virtual_host, route);
    let client = reqwest::Client::new();
    let request_body = HashMap::from([("url", url)]);
    println!("Setting the webhook to {:?}", request_body);
    let response = client
        .post(telegram_url.as_str())
        .json(&request_body)
        .send()
        .await?;
    println!("Telegram's response {}", response.text().await?);

    println!("Starting rocket");
    let _rocket = rocket::build()
        .manage(db)
        .manage(api)
        .mount(route, routes![index])
        .launch()
        .await?;

    //Stop the webhook
    println!("Stopping the webhook");
    client
        .post(format!(
            "https://api.telegram.org/bot{}/deleteWebhook?drop_pending_updates=True",
            token
        ))
        .send()
        .await?;
    Ok(())
}
