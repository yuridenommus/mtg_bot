use chrono::{DateTime, Utc};
use diesel::{Connection, PgConnection};
use mtg_bot::models::{InsertCard, InsertCardFace, InsertPreview};
use serde::de::{SeqAccess, Visitor};
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fs::File;
use std::marker::PhantomData;
use std::{env, fmt};

#[derive(Serialize, Deserialize, Debug)]
struct Bulk {
    object: String,
    id: String,
    #[serde(rename = "type")]
    data_type: String,
    updated_at: DateTime<Utc>,
    uri: String,
    download_uri: String,
    content_encoding: String,
}

struct CardIteratorAdapter(u32);

struct CardVisitor {
    phantom: PhantomData<fn() -> CardIteratorAdapter>,
    connection: PgConnection,
}

impl<'de> Visitor<'de> for CardVisitor {
    type Value = CardIteratorAdapter;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("A bulk of all cards from the Scryfall API")
    }

    fn visit_seq<S>(self, mut seq: S) -> Result<Self::Value, S::Error>
    where
        S: SeqAccess<'de>,
    {
        use diesel::prelude::*;
        let conn = &self.connection;
        let result = self
            .connection
            .transaction::<u32, Box<dyn Error>, _>(move || {
                let mut i = 0;
                while let Some(c) = seq.next_element()? {
                    {
                        use mtg_bot::schema::cards::dsl::*;

                        let card = InsertCard::new(&c);
                        diesel::insert_into(cards)
                            .values(&card)
                            .on_conflict(id)
                            .do_update()
                            .set(&card)
                            .execute(conn)?;
                    }
                    for (j, f) in (0..).zip(c.card_faces.iter()) {
                        use mtg_bot::schema::card_faces::dsl::*;

                        let card_face = InsertCardFace::new(&c.id, j, f);
                        diesel::insert_into(card_faces)
                            .values(&card_face)
                            .on_conflict((card_id, id))
                            .do_update()
                            .set(&card_face)
                            .execute(conn)?;
                    }
                    if let Some(ref p) = c.preview {
                        use mtg_bot::schema::previews::dsl::*;
                        let preview = InsertPreview::new(&c.id, p);
                        diesel::insert_into(previews)
                            .values(&preview)
                            .on_conflict(card_id)
                            .do_update()
                            .set(&preview)
                            .execute(conn)?;
                    }
                    i += 1;
                }
                Ok(i)
            })
            .map_err(serde::de::Error::custom)?;
        println!("Transaction finished");
        Ok(CardIteratorAdapter(result))
    }
}

impl<'de> Deserialize<'de> for CardIteratorAdapter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        println!("Starting");
        println!("Checking db url");
        let db_url = env::var("DATABASE_URL")
            .or_else(|_| dotenv::var("DATABASE_URL"))
            .map_err(serde::de::Error::custom)?;
        println!("db_path is {:?}", db_url);
        println!("Opening database");
        let db = PgConnection::establish(&db_url).map_err(serde::de::Error::custom)?;
        println!("Opened connection with database\n");
        let visitor = CardVisitor {
            phantom: PhantomData,
            connection: db,
        };
        deserializer.deserialize_seq(visitor)
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    dotenv::dotenv()?;

    let file_path = env::args().nth(1).ok_or("No path to cards file passed")?;
    let card_file = File::open(file_path)?;

    println!("Parse the cards");
    let CardIteratorAdapter(len) = serde_json::from_reader(&card_file)?;
    println!("Inserted {} cards in the database", len);
    println!("Finished process");
    Ok(())
}
