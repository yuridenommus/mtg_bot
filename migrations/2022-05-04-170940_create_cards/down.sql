DROP TABLE IF EXISTS card_faces;
DROP TABLE IF EXISTS previews;
DROP TABLE IF EXISTS cards;
DROP EXTENSION IF EXISTS pg_trgm;
