CREATE INDEX idx_card_lang ON cards USING HASH (lang);

CREATE INDEX idx_card_image_uris ON cards USING GIN (image_uris);
