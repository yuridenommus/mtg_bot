FROM nixpkgs/nix-flakes

WORKDIR /opt/mtg_bot

COPY Cargo.* ./

COPY flake.* ./

COPY rust-toolchain.toml ./

RUN nix build .\#diesel --no-link

CMD ["nix" "build"]
